Changelog
=========
0.1.4 (2021-12-17)
- add exit code on failure

0.1.3 (2021-08-12)
------------------
- changed wrong url in setup.py

0.1.2 (2021-08-12)
------------------

0.1.0 (2021-08-12) initial release
----------------------------------
